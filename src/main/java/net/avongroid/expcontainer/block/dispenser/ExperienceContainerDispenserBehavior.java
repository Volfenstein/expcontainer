package net.avongroid.expcontainer.block.dispenser;

import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;

import net.minecraft.nbt.NbtCompound;

import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPointer;

import net.minecraft.server.world.ServerWorld;

import net.minecraft.entity.ExperienceOrbEntity;

import net.minecraft.block.Block;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.ItemDispenserBehavior;

import net.avongroid.expcontainer.util.ExperienceStorageUtil;

import net.avongroid.expcontainer.block.ExperienceContainerBlock;

public class ExperienceContainerDispenserBehavior {
	public static ItemStack defaultBehavior (BlockPointer pointer, ItemStack stack) {
		return new ItemDispenserBehavior() {
			@Override
			public ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
				if (stack.getItem() instanceof BlockItem) {
					BlockItem blockItem = (BlockItem)stack.getItem();
					Block block = blockItem.getBlock();
					if (block instanceof ExperienceContainerBlock) {
						
						int xp = 0;
						int levels = 0;
						NbtCompound containerBlockItemNBT;
						
						if ((containerBlockItemNBT = stack.getSubNbt("BlockEntityTag")) != null) {
							if (containerBlockItemNBT.contains("XP")) {
								xp = containerBlockItemNBT.getInt("XP");
								levels = ExperienceStorageUtil.getLevelFromXP(xp);
								if (levels < 1)
									return stack;
							} else
								return stack;
						} else
							return stack;
						
						int decrementXP = levels == 1 ? ExperienceStorageUtil.getXPFromLevel(1) : ExperienceStorageUtil.getXPFromLevel(levels) - ExperienceStorageUtil.getXPFromLevel(levels - 1);
						int remainderXP = xp - decrementXP;
						
						ServerWorld world = pointer.getWorld();
						Direction dir = pointer.getBlockState().get(DispenserBlock.FACING);
						BlockPos pos = pointer.getPos();
						
						double x = pos.offset(dir).getX() + 0.5d;
						double y = pos.offset(dir).getY() + 0.5d;
						double z = pos.offset(dir).getZ() + 0.5d;
						
						double speed = 1d + world.random.nextDouble() * 0.05d;
						Vec3d velVec = new Vec3d(dir.getOffsetX(), dir.getOffsetY(), dir.getOffsetZ()).normalize().add(world.random.nextGaussian() * 0.0075d, world.random.nextGaussian() * 0.0075d, world.random.nextGaussian() * 0.0075d).multiply(speed);
						ExperienceOrbEntity orbEntity = new ExperienceOrbEntity(world, x, y, z, decrementXP);
						orbEntity.setVelocity(velVec);
						world.spawnEntity(orbEntity);
						
						containerBlockItemNBT.putInt("XP", remainderXP);
						stack.setSubNbt("BlockEntityTag", containerBlockItemNBT);
					}
				}
				return stack;
			}
		}.dispense(pointer, stack);
	}
}
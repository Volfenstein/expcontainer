package net.avongroid.expcontainer.block;

import java.util.List;
import java.util.Random;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.world.World;
import net.minecraft.world.BlockView;

import net.minecraft.nbt.NbtCompound;

import net.minecraft.state.StateManager;

import net.minecraft.util.Hand;
import net.minecraft.util.Formatting;
import net.minecraft.util.BlockMirror;
import net.minecraft.util.ActionResult;
import net.minecraft.util.BlockRotation;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;

import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundCategory;

import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Style;
import net.minecraft.text.TranslatableText;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FacingBlock;
import net.minecraft.block.BlockWithEntity;

import net.minecraft.util.hit.BlockHitResult;

import net.minecraft.block.entity.BlockEntity;

import net.minecraft.server.world.ServerWorld;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemPlacementContext;

import net.minecraft.entity.player.PlayerEntity;

import net.minecraft.client.item.TooltipContext;

import net.minecraft.enchantment.Enchantments;
import net.minecraft.enchantment.EnchantmentHelper;

import net.avongroid.expcontainer.reference.Reference;

import net.avongroid.expcontainer.ExperienceContainer;

import net.minecraft.state.property.Property;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.DirectionProperty;

import net.avongroid.expcontainer.util.ExperienceStorage;
import net.avongroid.expcontainer.util.ExperienceStorageUtil;

import net.avongroid.expcontainer.api.ECBEHelper;
import net.avongroid.expcontainer.api.UpgradeProvider;
import net.avongroid.expcontainer.api.ColorDurabilityBarProvider;

import net.avongroid.expcontainer.config.ExperienceContainerModConfig;

import net.avongroid.expcontainer.block.provider.interact.InteractEvent;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;

import net.avongroid.expcontainer.block.entity.ExperienceContainerBlockEntity;

public abstract class ExperienceContainerBlock extends BlockWithEntity implements ColorDurabilityBarProvider {
	
	public abstract ActionResult onInteract (ExperienceContainerBlockEntity xpContainerBlockEntity, BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit);
	public abstract boolean hasDroppedExperienceWithoutSilkTouch(World world, ItemStack stack, BlockPos pos, BlockState state, PlayerEntity player);
	public abstract boolean hasDroppedExperienceAfterExplode(World world, BlockPos pos, BlockState state);
	public abstract int getStorageCapacity();
	
	public static final DirectionProperty FACING;
	public static final BooleanProperty LOCKED;
	public static final BooleanProperty HAVE;
	public static final BooleanProperty LIT;
	
	static {
		LOCKED = BooleanProperty.of("locked");
		HAVE = BooleanProperty.of("have");
		LIT = BooleanProperty.of("lit");
		FACING = FacingBlock.FACING;
	}
	
	public static boolean nonSpawning (BlockState state, BlockView view, BlockPos pos, EntityType<?> type) {return false;}
	public static int defaultLightBehavior (BlockState state) {
		return state.get(LIT) & !state.get(LOCKED) ? 8 : 0;
	}
	
	protected ExperienceContainerBlock (FabricBlockSettings settings) {
		super(settings);
		
		this.setDefaultState(this.getDefaultState().with(LOCKED, false).with(HAVE, false).with(LIT, false).with(FACING, Direction.UP));
	}
	
	protected boolean upgradeStorage(InteractEvent event, UpgradeProvider<? extends ExperienceContainerBlock, ?> provider) {
		final PlayerEntity player = event.getPlayer();
		final BlockState state = event.getState();
		final BlockPos pos = event.getPos();
		
		final World world = player.getEntityWorld();
		
		boolean status = world.setBlockState(pos, provider.upgradeType().getDefaultState().with(FACING, state.get(FACING)).with(LIT, state.get(LIT)).with(HAVE, state.get(HAVE)));
		
		if (status != true) {
			throw new RuntimeException("BlockState can't be changed. Maybe he doesn't exist. [" + super.toString() + "]");
		}
		
		return true;
	}
	
	protected InteractEvent makeInteractEvent(ExperienceContainerBlockEntity xpContainerBlockEntity, PlayerEntity player, BlockState state, BlockPos pos) {
		return InteractEvent.make(xpContainerBlockEntity, player, state, pos);
	}

	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
		if (player.isSpectator())
			return ActionResult.PASS;
		if (hit.getSide() != state.get(FACING))
			return ActionResult.PASS;
		if (state.get(LOCKED))
			return ActionResult.SUCCESS;
		
		var result = ActionResult.CONSUME;
		var xpContainerBlockEntityOptional = ECBEHelper.getExperienceContainerBlockEntity(world, pos);
		
		if (xpContainerBlockEntityOptional.isEmpty() == false) {
			result = this.onInteract(xpContainerBlockEntityOptional.get(), state, world, pos, player, hand, hit);
			world.updateComparators(pos, this);
		}
		
		return result;
	}
	
	@Override
	public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
		if (state.isOf(newState.getBlock()))
			return;
		
		if (!world.isClient) {
			world.updateComparators(pos, state.getBlock());
		}
	}
	
	@Override
	@Environment(EnvType.CLIENT)
	public void appendTooltip(ItemStack stack, BlockView world, List<Text> tooltip, TooltipContext options) {
		NbtCompound nbt = stack.getSubNbt("BlockEntityTag");
		
		if (nbt != null && nbt.contains("XP") && nbt.contains("MaxXP")) {
			int xp = nbt.getInt("XP");
			int maxXP = nbt.getInt("MaxXP");
			int levels = ExperienceStorageUtil.getLevelFromXP(xp);
			int percents = (int)(100f * ((float)xp / (float)maxXP));
			
			TranslatableText xpTranslatableText = (TranslatableText)new TranslatableText("tooltip." + Reference.MODID + ".wooden_small_box.xp", xp, maxXP);
			TranslatableText levelsTranslatableText = (TranslatableText)new TranslatableText("tooltip." + Reference.MODID + ".wooden_small_box.levels", levels, ExperienceStorageUtil.getLevelFromXP(maxXP));
			
			Style style = Style.EMPTY.withColor(TextColor.fromRgb(durabilityBarColor()));
			
			tooltip.add(new LiteralText(String.format("%s%%", percents)).setStyle(style));
			tooltip.add(new LiteralText(" ").append(levelsTranslatableText.formatted(Formatting.LIGHT_PURPLE)));
			tooltip.add(new LiteralText(" ").append(xpTranslatableText.setStyle(style)));
		} else {
			TranslatableText xpTranslatableText = (TranslatableText)new TranslatableText("tooltip." + Reference.MODID + ".wooden_small_box.xp", 0, this.getStorageCapacity());
			TranslatableText levelsTranslatableText = (TranslatableText)new TranslatableText("tooltip." + Reference.MODID + ".wooden_small_box.levels", 0, ExperienceStorageUtil.getLevelFromXP(this.getStorageCapacity()));
			
			Style style = Style.EMPTY.withColor(TextColor.fromRgb(durabilityBarColor()));
			
			tooltip.add(new LiteralText(String.format("%s%%", 0)).setStyle(style));
			tooltip.add(new LiteralText(" ").append(levelsTranslatableText.formatted(Formatting.LIGHT_PURPLE)));
			tooltip.add(new LiteralText(" ").append(xpTranslatableText.setStyle(style)));
		}
	}
	
	public void onBlockExploded (BlockState state, World world, BlockPos pos) {
		if (!world.isClient()) {
			var xpContainerBlockEntityOptional = ECBEHelper.getExperienceContainerBlockEntity(world, pos);
			if (xpContainerBlockEntityOptional.isEmpty() == false) {
				if (this.hasDroppedExperienceAfterExplode(world, pos, state)) {
					float xp = (float)xpContainerBlockEntityOptional.get().getStorage().getXP();
					dropExperience(world.getServer().getOverworld(), pos, (int)(xp * 0.8f));
				}
				world.removeBlockEntity(pos);
			}
		}
	}
	
	@Override
	@Environment(EnvType.CLIENT)
	public ItemStack getPickStack(BlockView view, BlockPos pos, BlockState state) {
		var xpContainerBlockEntityOptional = ECBEHelper.getExperienceContainerBlockEntity(view, pos);
		ItemStack stack = super.getPickStack(view, pos, state);
		
		if (xpContainerBlockEntityOptional.isEmpty() == false) {
			ExperienceContainerBlockEntity xpContainerBlockEntity = xpContainerBlockEntityOptional.get();
			ExperienceStorage storage = xpContainerBlockEntity.getStorage();
			
			NbtCompound blockEntityNBT = new NbtCompound();
			blockEntityNBT.putInt("MaxXP", this.getStorageCapacity());
			blockEntityNBT.putInt("XP", xpContainerBlockEntity.getStorage().getXP());
			
			NbtCompound blockStateNBT = new NbtCompound();
			blockStateNBT.putBoolean("lit", !storage.isEmpty());
			blockStateNBT.putBoolean("have", !storage.isEmpty());
			
			stack.setSubNbt("BlockStateTag", blockStateNBT);
			stack.setSubNbt("BlockEntityTag", blockEntityNBT);
		}
		
		return stack;
	}
	
	@Override
	public void neighborUpdate(BlockState state, World world, BlockPos pos, Block block, BlockPos fromPos, boolean notify) {
		if (!world.isClient) {
			boolean lit = state.get(LIT);
			boolean haveContainer = false;
			boolean locked = state.get(LOCKED);
			
			var xpContainerBlockEntityOptional = ECBEHelper.getExperienceContainerBlockEntity(world, pos);
			
			if(xpContainerBlockEntityOptional.isEmpty() == false) {
				haveContainer = xpContainerBlockEntityOptional.get().getStorage().isEmpty() == false;
				if (lit != haveContainer) {
					if (lit) {
						world.createAndScheduleBlockTick(pos, this, 4);
					} else {
						world.setBlockState(pos, state.cycle(LIT).cycle(HAVE), Block.NOTIFY_LISTENERS);
					}
				}
				if (locked != world.isReceivingRedstonePower(pos)) {
					if (locked) {
						world.createAndScheduleBlockTick(pos, this, 4);
					} else {
						world.setBlockState(pos, state.cycle(LOCKED), Block.NOTIFY_LISTENERS);
					}
				}
			}
		}
	}
	
	@Override
	public void onPlaced(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack) {
		if (!world.isClient) {
			var xpContainerBlockEntityOptional = ECBEHelper.getExperienceContainerBlockEntity(world, pos);
			if (xpContainerBlockEntityOptional.isEmpty() == false) {
				final boolean haveContainer = !xpContainerBlockEntityOptional.get().getStorage().isEmpty();
				
				world.setBlockState(pos, state.with(LIT, haveContainer).with(HAVE, haveContainer));
			}
		}
	}
	
	@Override
	public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
		var xpContainerBlockEntityOptional = ECBEHelper.getExperienceContainerBlockEntity(world, pos);
		if (xpContainerBlockEntityOptional.isEmpty() == false) {
			if (state.get(LIT) & xpContainerBlockEntityOptional.get().getStorage().isEmpty())
				world.setBlockState(pos, state.cycle(LIT).cycle(HAVE), 3);
			if (state.get(LOCKED) != world.isReceivingRedstonePower(pos))
				world.setBlockState(pos, state.cycle(LOCKED), 3);
		}
	}
	
	@Override
	public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
		if (!world.isClient) {
			ECBEHelper.getExperienceContainerBlockEntity(world, pos).ifPresent((xpContainerBlockEntity) -> {
				ExperienceStorage storage = xpContainerBlockEntity.getStorage();
				final boolean empty = storage.isEmpty();
				final int maxXP = storage.getMaxXP();
				final int xp = storage.getXP();
				if (empty == false) {
					NbtCompound stateNBT;
					NbtCompound entityNBT;
					ItemEntity itemEntity;
					ItemStack itemStack = new ItemStack(this);
					if (player.isCreative()) {
						stateNBT = new NbtCompound();
						entityNBT = new NbtCompound();
						
						entityNBT.putInt("XP", xp);
						entityNBT.putInt("MaxXP", maxXP);
						itemStack.setSubNbt("BlockEntityTag", entityNBT);
						
						stateNBT.putString("lit", empty ? "false" : "true");
						stateNBT.putString("have", empty ? "false" : "true");
						itemStack.setSubNbt("BlockStateTag", stateNBT);
						
						itemEntity = new ItemEntity(world, (float)pos.getX() + 0.5f, (float)pos.getY() + 0.5f, (float)pos.getZ() + 0.5f, itemStack);
						itemEntity.setToDefaultPickupDelay();
						world.spawnEntity(itemEntity);
					} else {
						if (this.hasDroppedExperienceWithoutSilkTouch(world, itemStack, pos, state, player)) {
							boolean isSilkTouch = EnchantmentHelper.getLevel(Enchantments.SILK_TOUCH, player.getMainHandStack()) > 0;
							super.dropExperience(world.getServer().getOverworld(), pos, isSilkTouch ? 0 : (int)((float)xp * 0.8f));
						}
					}
				}
				world.removeBlockEntity(pos);
			});
		}
		super.onBreak(world, pos, state, player);
	}
	
	@Override
	public void appendProperties (StateManager.Builder<Block, BlockState> stateManager) {
		stateManager.add(new Property[] {FACING, HAVE, LIT, LOCKED});
	}
	
	protected void dingdong (World world, float x, float y, float z, SoundEvent soundEvent) {
		Random random = world.getRandom();
		float xx = x + 0.5f;
		float yy = y + 0.5f;
		float zz = z + 0.5f;
		
		world.playSound(null, xx, yy, zz, soundEvent, SoundCategory.BLOCKS, 0.5f + random.nextFloat() * 0.5f, random.nextFloat() - random.nextFloat());
	}
	
	protected void dingdong (World world, BlockPos position, SoundEvent soundEvent) {
		Random random = world.getRandom();
		float xx = (float) position.getX() + 0.5f;
		float yy = (float) position.getY() + 0.5f;
		float zz = (float) position.getZ() + 0.5f;
		
		world.playSound(null, xx, yy, zz, soundEvent, SoundCategory.BLOCKS, 0.5f + random.nextFloat() * 0.5f, random.nextFloat() - random.nextFloat());
	}
	
	@Override
	public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
		var xpContainerBlockEntityOptional = ECBEHelper.getExperienceContainerBlockEntity(world, pos);
		if (xpContainerBlockEntityOptional.isEmpty() == false) {
			ExperienceContainerModConfig config = ExperienceContainer.CONFIG;
			ExperienceStorage storage = xpContainerBlockEntityOptional.get().getStorage();
			
			final float currXP = storage.getXP();
			final float maxXP = storage.getMaxXP();
			
			return (int)((currXP / maxXP * (config.getMaxComparatorOutputWave() - 1)) + 1.0f * (currXP / (float)Math.max(1.0f, currXP)));
		}
		
		return 0;
	}
	
	@Override
	public BlockEntity createBlockEntity (BlockPos pos, BlockState state) {
		return new ExperienceContainerBlockEntity(pos, state, this.getStorageCapacity());
	}
	
	@Override
	public BlockState rotate(BlockState state, BlockRotation rotation) {
		return state.with(FACING, rotation.rotate(state.get(FACING)));
	}
	
	@Override
	public BlockState getPlacementState (ItemPlacementContext context) {
		final PlayerEntity player = context.getPlayer();
		final BlockPos pos = context.getBlockPos();
		final Vec3d playerPos = player.getPos();
		
		if (playerPos.y > pos.getY() || playerPos.y + 1.5f < pos.getY()) {
			return this.getDefaultState().with(FACING, context.getSide()).with(LOCKED, context.getWorld().isReceivingRedstonePower(pos));
		}
		
		return this.getDefaultState().with(FACING, player.getHorizontalFacing().getOpposite()).with(LOCKED, context.getWorld().isReceivingRedstonePower(pos));
	}
	
	@Override
	public BlockState mirror(BlockState state, BlockMirror mirror) {
		return state.rotate(mirror.getRotation(state.get(FACING)));
	}
	
	@Override
	public boolean hasComparatorOutput(BlockState state) {
		return true;
	}
}
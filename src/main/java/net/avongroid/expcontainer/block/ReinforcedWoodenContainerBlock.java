package net.avongroid.expcontainer.block;

import net.minecraft.world.World;

import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;

import net.minecraft.block.Material;
import net.minecraft.block.BlockState;

import net.minecraft.util.math.BlockPos;

import net.minecraft.sound.BlockSoundGroup;

import net.minecraft.entity.player.PlayerEntity;

import net.avongroid.expcontainer.reference.Reference;

import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;

public class ReinforcedWoodenContainerBlock extends ExperienceContainerBox {
	
	public static final ReinforcedWoodenContainerBlock INSTANCE = new ReinforcedWoodenContainerBlock();
	
	@SuppressWarnings("removal")
	public ReinforcedWoodenContainerBlock() {
		super(FabricBlockSettings.of(Material.METAL)
			.luminance(ExperienceContainerBlock::defaultLightBehavior)
			.allowsSpawning(ExperienceContainerBlock::nonSpawning)
			.breakByTool(FabricToolTags.PICKAXES)
			.sounds(BlockSoundGroup.METAL)
			.strength(6.0f, 3600000.0f)
			.nonOpaque()
		);
	}
	
	@Override
	public boolean hasDroppedExperienceWithoutSilkTouch(World world, ItemStack stack, BlockPos pos, BlockState state, PlayerEntity player) {
		return false;
	}

	@Override
	public boolean hasDroppedExperienceAfterExplode(World world, BlockPos pos, BlockState state) {
		return false;
	}
	
	@Override
	public BlockItem getBlockItem (final BlockItem.Settings settings) {
		settings.rarity(Reference.getReinforcedWoodenContainerBoxRarity());
		settings.fireproof();
		
		return new BlockItem(this, settings);
	}

	@Override
	public int getStorageCapacity() {
		return Reference.getStorageCapacity();
	}
}
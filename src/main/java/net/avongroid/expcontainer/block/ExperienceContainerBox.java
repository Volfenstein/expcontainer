package net.avongroid.expcontainer.block;

import net.minecraft.util.Hand;

import net.minecraft.world.World;

import net.minecraft.item.BlockItem;

import net.minecraft.block.BlockState;
import net.minecraft.block.BlockRenderType;

import net.minecraft.sound.SoundEvents;

import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.hit.BlockHitResult;

import net.minecraft.entity.player.PlayerEntity;

import net.avongroid.expcontainer.util.ExperienceStorage;
import net.avongroid.expcontainer.util.ExperienceStorageUtil;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;

import net.avongroid.expcontainer.block.entity.ExperienceContainerBlockEntity;

public abstract class ExperienceContainerBox extends ExperienceContainerBlock {
	
	public abstract BlockItem getBlockItem(final BlockItem.Settings settings);
	
	private int durabilityBarColor = 0x00ff00;
	
	public ExperienceContainerBox(FabricBlockSettings settings) {
		super (settings);
	}
	
	@Override
	public ActionResult onInteract(ExperienceContainerBlockEntity xpContainerBlockEntity, BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
		if (!world.isClient) {
			ExperienceStorage storage = xpContainerBlockEntity.getStorage();
			int containerXP = storage.getXP();
			
			int playerXP = ExperienceStorageUtil.getPlayerXP(player);
			
			if (playerXP < 1 && containerXP < 1)
				return ActionResult.SUCCESS;
			
			if (player.isSneaking()) {
				if (!storage.isEmpty()) {
					xpContainerBlockEntity.addExperience(player, containerXP);
					dingdong(world, pos, SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP);
				}
			} else {
				if (!storage.isFull()) {
					if (playerXP > 0) {
						xpContainerBlockEntity.removeExperience(player, playerXP);
						dingdong(world, pos, SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP);
					}
				}
			}
			
			containerXP = xpContainerBlockEntity.getStorage().getXP();
			playerXP = ExperienceStorageUtil.getPlayerXP(player);
			final boolean haveContainer = containerXP > 0;
			
			world.setBlockState(pos, state.with(LIT, haveContainer).with(HAVE, haveContainer), 2);
		}
		
		return ActionResult.SUCCESS;
	}
	
	@Override
	public ExperienceContainerBlock durabilityBarColor(int color) {
		durabilityBarColor = color;
		return this;
	}
	
	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.MODEL;
	}
	
	@Override
	public int durabilityBarColor() {
		return durabilityBarColor;
	}
}
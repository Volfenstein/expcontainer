package net.avongroid.expcontainer.block.provider.interact;

import net.minecraft.block.BlockState;

import net.minecraft.util.math.BlockPos;

import net.minecraft.entity.player.PlayerEntity;

import net.avongroid.expcontainer.block.entity.ExperienceContainerBlockEntity;

public class InteractEvent {
	
	private ExperienceContainerBlockEntity xpContainerBlockEntity;
	private PlayerEntity player;
	private BlockState state;
	private BlockPos pos;
	
	private InteractEvent (ExperienceContainerBlockEntity xpContainerBlockEntity, PlayerEntity player, BlockState state, BlockPos pos) {
		this.xpContainerBlockEntity = xpContainerBlockEntity;
		this.player = player;
		this.state = state;
		this.pos = pos;
	}
	
	public ExperienceContainerBlockEntity getExperienceContainerBlockEntity() {
		return xpContainerBlockEntity;
	}

	public PlayerEntity getPlayer() {
		return player;
	}

	public BlockState getState() {
		return state;
	}

	public BlockPos getPos() {
		return pos;
	}
	
	public static InteractEvent make(ExperienceContainerBlockEntity xpContainerBlockEntity, PlayerEntity player, BlockState state, BlockPos pos) {
		return new InteractEvent(xpContainerBlockEntity, player, state, pos);
	}
}
package net.avongroid.expcontainer.block;

import net.minecraft.world.World;

import net.minecraft.item.Items;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;

import net.minecraft.block.Material;
import net.minecraft.block.BlockState;

import net.minecraft.sound.SoundEvents;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.BlockSoundGroup;

import net.minecraft.util.Hand;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.hit.BlockHitResult;

import net.minecraft.entity.player.PlayerEntity;

import net.avongroid.expcontainer.api.FuelProvider;
import net.avongroid.expcontainer.api.UpgradeProvider;

import net.avongroid.expcontainer.reference.Reference;

import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;

import net.avongroid.expcontainer.block.entity.ExperienceContainerBlockEntity;

public class WoodenContainerBlock extends ExperienceContainerBox implements UpgradeProvider<ReinforcedWoodenContainerBlock, WoodenContainerBlock>, FuelProvider {
	
	public static final WoodenContainerBlock INSTANCE = new WoodenContainerBlock();
	
	private ReinforcedWoodenContainerBlock upgradeType;
	
	@SuppressWarnings("removal")
	public WoodenContainerBlock () {
		super(FabricBlockSettings.of(Material.WOOD)
			.luminance(ExperienceContainerBlock::defaultLightBehavior)
			.allowsSpawning(ExperienceContainerBlock::nonSpawning)
			.breakByTool(FabricToolTags.AXES, 3)
			.sounds(BlockSoundGroup.WOOD)
			.strength(2.0f, 15.0f)
			.nonOpaque()
		);
	}
	
	@Override
	public ActionResult onInteract(ExperienceContainerBlockEntity xpContainerBlockEntity, BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
		final ItemStack NETHERITE_INGOT_STACK = new ItemStack(Items.NETHERITE_INGOT);
		final ItemStack HAND_STACK = player.getStackInHand(hand);
		
		if (HAND_STACK.isItemEqual(NETHERITE_INGOT_STACK)) {
			if (upgradeStorage(makeInteractEvent(xpContainerBlockEntity, player, state, pos), this) == true) {
				if (player.isCreative() == false) {
					HAND_STACK.decrement(1);
				}
				
				world.playSound(null, pos, SoundEvents.BLOCK_NETHERITE_BLOCK_HIT, SoundCategory.BLOCKS, 1.0F, 1.0F);
				return ActionResult.SUCCESS;
			}
		}
		
		return super.onInteract(xpContainerBlockEntity, state, world, pos, player, hand, hit);
	}
	
	@Override
	public boolean hasDroppedExperienceWithoutSilkTouch(World world, ItemStack stack, BlockPos pos, BlockState state, PlayerEntity player) {
		return true;
	}
	
	@Override
	public boolean hasDroppedExperienceAfterExplode(World world, BlockPos pos, BlockState state) {
		return true;
	}
	
	@Override
	public WoodenContainerBlock upgradeType(ReinforcedWoodenContainerBlock upgradeType) {
		this.upgradeType = upgradeType;
		return this;
	}
	
	@Override
	public BlockItem getBlockItem (final BlockItem.Settings settings) {
		settings.rarity(Reference.getWoodenContainerBoxRarity());
		
		return new BlockItem(this, settings);
	}
	
	@Override
	public ReinforcedWoodenContainerBlock upgradeType() {
		return this.upgradeType;
	}
	
	@Override
	public int getStorageCapacity() {
		return Reference.getStorageCapacity();
	}
	
	@Override
	public int getBurnTime() {
		return 2290;
	}
}
package net.avongroid.expcontainer.block.entity;

import net.minecraft.util.registry.Registry;

import net.minecraft.block.entity.BlockEntityType;

import net.avongroid.expcontainer.reference.Reference;

import net.avongroid.expcontainer.block.WoodenContainerBlock;
import net.avongroid.expcontainer.block.ExperienceContainerBlock;
import net.avongroid.expcontainer.block.ReinforcedWoodenContainerBlock;

import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;

public class ExperienceContainerBlockEntityType {
	
	public static BlockEntityType<ExperienceContainerBlockEntity> EXPERIENCE_CONTAINER_BLOCK_ENTITY;
	
	public static final void registerAll () {
		ExperienceContainerBlockEntityType.EXPERIENCE_CONTAINER_BLOCK_ENTITY = register (
			WoodenContainerBlock.INSTANCE, ReinforcedWoodenContainerBlock.INSTANCE
		);
	}
	
	private static final BlockEntityType<ExperienceContainerBlockEntity> register (ExperienceContainerBlock... blocks) {
		BlockEntityType<ExperienceContainerBlockEntity> EXPERIENCE_CONTAINER_BLOCK_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, Reference.MODID + ":experience_container_block_entity", FabricBlockEntityTypeBuilder.<ExperienceContainerBlockEntity>create(ExperienceContainerBlockEntity::new, blocks).build(null));
		return EXPERIENCE_CONTAINER_BLOCK_ENTITY;
	}
}
package net.avongroid.expcontainer.block.entity;

import net.minecraft.nbt.NbtCompound;

import net.minecraft.block.BlockState;

import net.minecraft.util.math.BlockPos;

import net.minecraft.entity.player.PlayerEntity;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;

import net.avongroid.expcontainer.reference.Reference;

import net.avongroid.expcontainer.util.ExperienceStorage;
import net.avongroid.expcontainer.util.ExperienceStorageUtil;
import net.avongroid.expcontainer.util.ExperienceStorageProvider;

public class ExperienceContainerBlockEntity extends BlockEntity implements ExperienceStorageProvider {
	
	private ExperienceStorage storage;
	
	public ExperienceContainerBlockEntity (BlockEntityType<? extends ExperienceContainerBlockEntity> blockEntityType, BlockPos pos, BlockState state, int storageCapacity) {
		super(blockEntityType, pos, state);
		storage = new ExperienceStorage(storageCapacity);
	}
	
	public ExperienceContainerBlockEntity (BlockEntityType<? extends ExperienceContainerBlockEntity> blockEntityType, BlockPos pos, BlockState state) {
		this(blockEntityType, pos, state, Reference.getStorageCapacity());
	}
	
	public ExperienceContainerBlockEntity (BlockPos pos, BlockState state, int storageCapacity) {
		this(ExperienceContainerBlockEntityType.EXPERIENCE_CONTAINER_BLOCK_ENTITY, pos, state, storageCapacity);
	}
	
	public ExperienceContainerBlockEntity (BlockPos pos, BlockState state) {
		this(ExperienceContainerBlockEntityType.EXPERIENCE_CONTAINER_BLOCK_ENTITY, pos, state, Reference.getStorageCapacity());
	}
	
	public void addExperience (PlayerEntity player, int xp) {
		if (xp < 0) return;
		
		int remainder = storage.getXP() - xp;
		
		ExperienceStorageUtil.addPlayerXP(player, remainder < 1 ? storage.getXP() : xp);
		storage.subtract(storage.getXP()).sum(remainder < 1 ? 0 : remainder);
	}
	
	public void removeExperience (PlayerEntity player, int xp) {
		if (xp < 0) return;
		
		int remainder = xp + storage.getXP() - storage.getMaxXP();
		
		storage.sum(remainder > 0 ? -storage.getXP() : xp).sum(remainder > 0 ? storage.getMaxXP() : 0);
		ExperienceStorageUtil.addPlayerXP(player, -xp);
		ExperienceStorageUtil.addPlayerXP(player, remainder);
	}
	
	@Override
	public void writeNbt (NbtCompound nbt) {
		super.writeNbt(nbt);
		
		nbt.putInt("XP", storage.getXP());
		nbt.putInt("MaxXP", storage.getMaxXP());
	}
	
	@Override
	public void readNbt (NbtCompound nbt) {
		super.readNbt(nbt);
		
		storage.xp = (nbt.contains("XP") ? nbt.getInt("XP") : 0);
		storage.setMaxXP(nbt.contains("MaxXP") ? nbt.getInt("MaxXP") : 0);
	}
	
	@Override
	public boolean hasExperienceStorage() {
		return !(storage == null);
	}
	
	@Override
	public ExperienceStorage getStorage() {
		return storage;
	}
	
	@Override
	public int getStorageCapacity() {
		return storage.getMaxXP();
	}
}
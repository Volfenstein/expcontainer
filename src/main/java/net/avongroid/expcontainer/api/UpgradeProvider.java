package net.avongroid.expcontainer.api;

import net.avongroid.expcontainer.block.ExperienceContainerBlock;
/*
 * S - Self type
 * CB - Container Block upgrade type
 */
public interface UpgradeProvider<CB extends ExperienceContainerBlock, S> {
	//Getter
	CB upgradeType ();
	//Setter
	S upgradeType (CB upgradeType);
}
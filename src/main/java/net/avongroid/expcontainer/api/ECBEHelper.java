package net.avongroid.expcontainer.api;

import java.util.Optional;

import net.minecraft.world.BlockView;

import net.minecraft.util.math.BlockPos;

import net.minecraft.block.entity.BlockEntity;

import net.avongroid.expcontainer.ExperienceContainer;

import net.avongroid.expcontainer.util.ExperienceStorage;
import net.avongroid.expcontainer.util.DurabilityDelimiterUtil;

import net.avongroid.expcontainer.block.entity.ExperienceContainerBlockEntity;

public class ECBEHelper {
	
	public static Optional<ExperienceContainerBlockEntity> getExperienceContainerBlockEntity (BlockView world, BlockPos position) {
		BlockEntity blockEntity = world.getBlockEntity(position);
		
		if (blockEntity != null & blockEntity instanceof ExperienceContainerBlockEntity)
			return Optional.of((ExperienceContainerBlockEntity)blockEntity);
		
		return Optional.empty();
	}
	
	public static int delimiters (ExperienceContainerBlockEntity xpContainerBlockEntity) {
		ExperienceStorage storage = xpContainerBlockEntity.getStorage();
		return DurabilityDelimiterUtil.calcDelimiters(storage.getMaxXP(), storage.getXP(), ExperienceContainer.CONFIG.getMaxDurabilityProgressDelimiters());
	}
	
}
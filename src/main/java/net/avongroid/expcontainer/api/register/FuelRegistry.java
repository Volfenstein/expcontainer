package net.avongroid.expcontainer.api.register;

import net.minecraft.item.BlockItem;

import net.fabricmc.fabric.impl.content.registry.FuelRegistryImpl;

public class FuelRegistry {
	
	public static final void register (BlockItem item, int burnTime) {
		FuelRegistryImpl.INSTANCE.add(item, burnTime);
	}
	
}

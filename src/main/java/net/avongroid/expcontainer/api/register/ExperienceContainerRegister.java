package net.avongroid.expcontainer.api.register;

import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;

import net.minecraft.block.DispenserBlock;

import net.minecraft.util.Rarity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.avongroid.expcontainer.api.FuelProvider;

import net.avongroid.expcontainer.util.Identifiers;

import net.avongroid.expcontainer.block.ExperienceContainerBox;

import net.avongroid.expcontainer.block.dispenser.ExperienceContainerDispenserBehavior;

public class ExperienceContainerRegister {
	
	public final String MODID;
	
	private ExperienceContainerRegister(final String MODID) {
		this.MODID = MODID;
	}
	
	public void registerBox(final ExperienceContainerBox BOX, final String ID) {
		BlockItem.Settings settings = new BlockItem.Settings().maxCount(1).rarity(Rarity.COMMON).group(ItemGroup.MISC);
		final BlockItem BLOCK_ITEM = BOX.getBlockItem(settings);
		
		if (BOX instanceof FuelProvider)
			FuelRegistry.register(BLOCK_ITEM, ((FuelProvider)BOX).getBurnTime());
		
		DispenserBlock.registerBehavior(BLOCK_ITEM, ExperienceContainerDispenserBehavior::defaultBehavior);
		
		final Identifier IDENTIFIER = Identifiers.of(MODID, ID);
		
		Registry.register(Registry.BLOCK, IDENTIFIER, BOX);
		Registry.register(Registry.ITEM, IDENTIFIER, BLOCK_ITEM);
	}
	
	public static ExperienceContainerRegister init (final String MODID) {
		return new ExperienceContainerRegister(MODID);
	}
}

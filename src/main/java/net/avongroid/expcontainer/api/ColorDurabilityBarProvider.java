package net.avongroid.expcontainer.api;

import net.avongroid.expcontainer.block.ExperienceContainerBlock;

public interface ColorDurabilityBarProvider {
	ExperienceContainerBlock durabilityBarColor(int color);
	int durabilityBarColor();
}
package net.avongroid.expcontainer.api;

public interface FuelProvider {
	int getBurnTime();
}
package net.avongroid.expcontainer.reference;

import net.minecraft.util.Rarity;

import net.avongroid.expcontainer.util.ExperienceStorageUtil;

public class Reference {
	
	public static final String VERSION = "1.15.3";
	public static final String MODID = "expcontainer";
	public static final String AUTHOR = "ModderCoder (Minecraft name : Avongroid)";
	
	public static final Rarity getReinforcedWoodenContainerBoxRarity() {
		return Rarity.RARE;
	}
	
	public static final Rarity getWoodenContainerBoxRarity() {
		return Rarity.UNCOMMON;
	}
	
	public static final int getStorageCapacity () {
		return ExperienceStorageUtil.getXPFromLevel(32);
	}
	
}

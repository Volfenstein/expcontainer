package net.avongroid.expcontainer;

import net.fabricmc.api.ModInitializer;

import net.avongroid.expcontainer.boxes.Boxes;

import net.avongroid.expcontainer.reference.Reference;

import net.avongroid.expcontainer.config.ExperienceContainerModConfig;

import net.avongroid.expcontainer.api.register.ExperienceContainerRegister;

import net.avongroid.expcontainer.block.entity.ExperienceContainerBlockEntityType;

import net.avongroid.expcontainer.config.register.ExperienceContainerConfigRegister;

public class ExperienceContainer implements ModInitializer {
	
	public static final ExperienceContainerRegister REGISTER = ExperienceContainerRegister.init(Reference.MODID);
	public static final ExperienceContainerModConfig CONFIG = ExperienceContainerConfigRegister.registerConfig(ExperienceContainerModConfig.class);
	
    @Override
    public void onInitialize () {
    	ExperienceContainerBlockEntityType.registerAll();
    	
    	REGISTER.registerBox(Boxes.OAK_BOX, "oak_small_box");
    	REGISTER.registerBox(Boxes.BIRCH_BOX, "birch_small_box");
    	REGISTER.registerBox(Boxes.SPRUCE_BOX, "spruce_small_box");
    	REGISTER.registerBox(Boxes.ACACIA_BOX, "acacia_small_box");
    	REGISTER.registerBox(Boxes.JUNGLE_BOX, "jungle_small_box");
    	REGISTER.registerBox(Boxes.WARPED_BOX, "warped_small_box");
    	REGISTER.registerBox(Boxes.CRIMSON_BOX, "crimson_small_box");
    	REGISTER.registerBox(Boxes.DARK_OAK_BOX, "dark_oak_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_OAK_BOX, "reinforced_oak_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_BIRCH_BOX, "reinforced_birch_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_SPRUCE_BOX, "reinforced_spruce_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_ACACIA_BOX, "reinforced_acacia_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_JUNGLE_BOX, "reinforced_jungle_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_WARPED_BOX, "reinforced_warped_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_CRIMSON_BOX, "reinforced_crimson_small_box");
    	REGISTER.registerBox(Boxes.REINFORCED_DARK_OAK_BOX, "reinforced_dark_oak_small_box");
    }
}
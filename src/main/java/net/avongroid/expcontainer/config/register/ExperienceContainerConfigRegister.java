package net.avongroid.expcontainer.config.register;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;

import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;

public class ExperienceContainerConfigRegister {
	
	public static synchronized <C extends ConfigData> C registerConfig (Class<C> classData) {
		AutoConfig.register(classData, GsonConfigSerializer::new);
		return (C) AutoConfig.getConfigHolder(classData).getConfig();
	}
	
}

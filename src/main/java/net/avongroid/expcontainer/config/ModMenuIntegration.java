package net.avongroid.expcontainer.config;

import java.util.Optional;

import net.fabricmc.api.EnvType;

import java.util.function.Supplier;

import net.fabricmc.api.Environment;

import org.jetbrains.annotations.Nullable;

import me.shedaniel.autoconfig.AutoConfig;

import net.minecraft.client.gui.screen.Screen;

import com.terraformersmc.modmenu.api.ModMenuApi;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;

@Environment(EnvType.CLIENT)
public class ModMenuIntegration implements ModMenuApi {
	
	@Override
	public ConfigScreenFactory<?> getModConfigScreenFactory() {
		return (@Nullable var screen) -> {
			Optional<Supplier<Screen>> optional = this.getConfigScreen(screen);
			return optional.isPresent() ? optional.get().get() : screen;
		};
	}
	
	private Optional<Supplier<Screen>> getConfigScreen(Screen screen) {
		return Optional.of(AutoConfig.getConfigScreen(ExperienceContainerModConfig.class, screen));
	}
	
}
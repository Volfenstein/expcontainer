package net.avongroid.expcontainer.config;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import net.avongroid.expcontainer.reference.Reference;

@Config(name = Reference.MODID)
public class ExperienceContainerModConfig implements ConfigData {
	
	@ConfigEntry.BoundedDiscrete(min = 6, max = 13)
	private int maxDurabilityProgressDelimiters = 13;
	
	private boolean useCustomDurabilityBarColors = false;
	
	@ConfigEntry.ColorPicker
	private int customDurabilityColor;
	
	@ConfigEntry.Category(value = "server_settings")
	@ConfigEntry.BoundedDiscrete(min = 0, max = 15)
	private int maxComparatorOutputWave = 6;
	
	public int getMaxDurabilityProgressDelimiters () {
		return maxDurabilityProgressDelimiters;
	}
	
	public boolean isUseCustomDurabilityBarColors () {
		return useCustomDurabilityBarColors;
	}
	
	public int getMaxComparatorOutputWave () {
		return maxComparatorOutputWave;
	}
	
	public int getCustomDurabilityColor () {
		return customDurabilityColor;
	}
}

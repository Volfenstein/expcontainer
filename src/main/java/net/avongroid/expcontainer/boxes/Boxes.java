package net.avongroid.expcontainer.boxes;

import net.avongroid.expcontainer.block.WoodenContainerBlock;
import net.avongroid.expcontainer.block.ReinforcedWoodenContainerBlock;

public class Boxes {
	public static final ReinforcedWoodenContainerBlock REINFORCED_OAK_BOX = new ReinforcedWoodenContainerBlock();
	public static final ReinforcedWoodenContainerBlock REINFORCED_BIRCH_BOX = new ReinforcedWoodenContainerBlock();
	public static final ReinforcedWoodenContainerBlock REINFORCED_SPRUCE_BOX = new ReinforcedWoodenContainerBlock();
	public static final ReinforcedWoodenContainerBlock REINFORCED_ACACIA_BOX = new ReinforcedWoodenContainerBlock();
	public static final ReinforcedWoodenContainerBlock REINFORCED_DARK_OAK_BOX = new ReinforcedWoodenContainerBlock();
	public static final ReinforcedWoodenContainerBlock REINFORCED_JUNGLE_BOX = ((ReinforcedWoodenContainerBlock)new ReinforcedWoodenContainerBlock().durabilityBarColor(0xf0fd8c));
	public static final ReinforcedWoodenContainerBlock REINFORCED_WARPED_BOX = ((ReinforcedWoodenContainerBlock)new ReinforcedWoodenContainerBlock().durabilityBarColor(0x75cdff));
	public static final ReinforcedWoodenContainerBlock REINFORCED_CRIMSON_BOX = ((ReinforcedWoodenContainerBlock)new ReinforcedWoodenContainerBlock().durabilityBarColor(0xf14040));
	
	public static final WoodenContainerBlock OAK_BOX = new WoodenContainerBlock().upgradeType(REINFORCED_OAK_BOX);
	public static final WoodenContainerBlock BIRCH_BOX = new WoodenContainerBlock().upgradeType(REINFORCED_BIRCH_BOX);
	public static final WoodenContainerBlock SPRUCE_BOX = new WoodenContainerBlock().upgradeType(REINFORCED_SPRUCE_BOX);
	public static final WoodenContainerBlock ACACIA_BOX = new WoodenContainerBlock().upgradeType(REINFORCED_ACACIA_BOX);
	public static final WoodenContainerBlock DARK_OAK_BOX = new WoodenContainerBlock().upgradeType(REINFORCED_DARK_OAK_BOX);
	public static final WoodenContainerBlock JUNGLE_BOX = ((WoodenContainerBlock)new WoodenContainerBlock().durabilityBarColor(0xf0fd8c)).upgradeType(REINFORCED_JUNGLE_BOX);
	public static final WoodenContainerBlock WARPED_BOX = ((WoodenContainerBlock)new WoodenContainerBlock().durabilityBarColor(0x75cdff)).upgradeType(REINFORCED_WARPED_BOX);
	public static final WoodenContainerBlock CRIMSON_BOX = ((WoodenContainerBlock)new WoodenContainerBlock().durabilityBarColor(0xf14040)).upgradeType(REINFORCED_CRIMSON_BOX);
}
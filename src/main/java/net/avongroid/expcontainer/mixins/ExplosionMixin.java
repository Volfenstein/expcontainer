package net.avongroid.expcontainer.mixins;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;

import net.minecraft.util.math.BlockPos;

import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion;

import net.avongroid.expcontainer.block.ExperienceContainerBlock;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Explosion.class)
public class ExplosionMixin {
	
	@Final
	@Shadow
	private World world;
	@Final
	@Shadow
	private List<BlockPos> affectedBlocks;
	
	@Inject(method = "affectWorld", at = @At(value = "HEAD"))
	private void affectWorld (CallbackInfo callback) {
		affectedBlocks.forEach((pos) -> {
			BlockState state = this.world.getBlockState(pos);
			Block block = state.getBlock();
			if (block != null && block instanceof ExperienceContainerBlock)
				((ExperienceContainerBlock)block).onBlockExploded(state, world, pos);
		});
	}
}

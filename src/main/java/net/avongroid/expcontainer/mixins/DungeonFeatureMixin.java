package net.avongroid.expcontainer.mixins;

import java.util.Random;

import net.minecraft.block.Block;

import net.minecraft.tag.Tag;
import net.minecraft.tag.BlockTags;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import net.minecraft.world.StructureWorldAccess;

import net.avongroid.expcontainer.api.ECBEHelper;

import net.avongroid.expcontainer.util.Identifiers;

import net.avongroid.expcontainer.reference.Reference;

import net.minecraft.world.gen.feature.DungeonFeature;
import net.minecraft.world.gen.feature.util.FeatureContext;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;

import net.avongroid.expcontainer.block.WoodenContainerBlock;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(DungeonFeature.class)
public class DungeonFeatureMixin {
	@Inject(method = "generate", at = @At("TAIL"), cancellable = true)
	public void generate (FeatureContext<DefaultFeatureConfig> context, CallbackInfoReturnable<Boolean> callback) {
		Tag<Block> tag = BlockTags.getTagGroup().getTag(Identifiers.of(Reference.MODID, "wooden_small_boxes"));
		WoodenContainerBlock containerBlock = (WoodenContainerBlock)tag.getRandom(context.getRandom());
		StructureWorldAccess structureWorldAccess = context.getWorld();
		Random random = context.getRandom();
		
		if (random.nextInt(3) != 2)
			return;
		
		final int MAX_STORAGE_CAPACITY = Reference.getStorageCapacity();
		BlockPos containerBlockPos = context.getOrigin().add(random.nextInt(2) + 2 * (random.nextInt(2) == 1 ? 1 : -1), 0, random.nextInt(2) + 2 * (random.nextInt(2) == 1 ? 1 : -1));
		
		int storageLevel = random.nextInt(MAX_STORAGE_CAPACITY);
		
		structureWorldAccess.setBlockState(containerBlockPos, containerBlock.getDefaultState(), 2);
		final Direction[] DIRS = {Direction.UP, Direction.EAST, Direction.WEST, Direction.NORTH, Direction.SOUTH};
		
		ECBEHelper.getExperienceContainerBlockEntity(structureWorldAccess, containerBlockPos).ifPresent((xpContainerBlockEntity) -> {
			Direction dir = DIRS[random.nextInt(DIRS.length)];
			if (!structureWorldAccess.isAir(containerBlockPos.offset(dir)))
				dir = dir.getOpposite();
			xpContainerBlockEntity.getStorage().sum(storageLevel);
			structureWorldAccess.setBlockState(containerBlockPos, containerBlock.getDefaultState()
				.with(WoodenContainerBlock.HAVE, storageLevel > 0 ? true : false)
				.with(WoodenContainerBlock.LIT, storageLevel > 0 ? true : false)
				.with(WoodenContainerBlock.FACING, dir), Block.NOTIFY_LISTENERS
			);
			callback.setReturnValue(true);
		});
		
		return;
	}
}
package net.avongroid.expcontainer.mixins;

import net.minecraft.block.Block;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;

import net.minecraft.nbt.NbtCompound;

import net.minecraft.client.font.TextRenderer;

import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.BufferBuilder;

import net.minecraft.client.render.item.ItemRenderer;

import net.avongroid.expcontainer.ExperienceContainer;

import net.avongroid.expcontainer.util.DurabilityDelimiterUtil;

import net.avongroid.expcontainer.block.ExperienceContainerBlock;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.avongroid.expcontainer.config.ExperienceContainerModConfig;

@Mixin(ItemRenderer.class)
@Environment(EnvType.CLIENT)
public class ItemRendererMixin {
	
	@Shadow
	private void renderGuiQuad(BufferBuilder buffer, int x, int y, int width, int height, int red, int green, int blue, int alpha) {}
	
	@Inject(method = "renderGuiItemOverlay(Lnet/minecraft/client/font/TextRenderer;Lnet/minecraft/item/ItemStack;IILjava/lang/String;)V", at = @At("HEAD"))
	private void renderDurabilityBar (TextRenderer textRenderer, ItemStack stack, int x, int y, String countLabel, CallbackInfo callback) {
		if (stack.getItem() instanceof BlockItem) {
			BlockItem blockItem = (BlockItem)stack.getItem();
			Block block = blockItem.getBlock();
			if (block instanceof ExperienceContainerBlock) {
				ExperienceContainerBlock xpContainerBlock = (ExperienceContainerBlock) block;
				ExperienceContainerModConfig config = ExperienceContainer.CONFIG;
				
				int delimiters = 0;
				NbtCompound containerBlockItemNBT;
				int maxDelimiters = config.getMaxDurabilityProgressDelimiters();
				
				if ((containerBlockItemNBT = stack.getSubNbt("BlockEntityTag")) != null) {
					int xp = containerBlockItemNBT.contains("XP") ? containerBlockItemNBT.getInt("XP") : 0;
					int maxXP = containerBlockItemNBT.contains("MaxXP") ? containerBlockItemNBT.getInt("MaxXP") : 0;
					delimiters = DurabilityDelimiterUtil.calcDelimiters(maxXP, xp, maxDelimiters);
					
					if (xp <= 0 || xp == maxXP)
						return;
				} else {
					return;
				}
				
				RenderSystem.disableBlend();
				RenderSystem.disableTexture();
				RenderSystem.disableDepthTest();
				Tessellator tessellator = Tessellator.getInstance();
				BufferBuilder bufferBuilder = tessellator.getBuffer();
				int length = (int)((float)delimiters/(float)maxDelimiters*13f);
				int color = config.isUseCustomDurabilityBarColors() ? config.getCustomDurabilityColor() : xpContainerBlock.durabilityBarColor();
				int r = color >> 16;
				int g = color >> 8 & 0xff;
				int b = color & 0xff;
				renderGuiQuad(bufferBuilder, x + 2, y + 13, 13, 2, 0, 0, 0, 255);
				renderGuiQuad(bufferBuilder, x + 2, y + 13, length, 1, r, g, b, 255);
				RenderSystem.enableBlend();
				RenderSystem.enableTexture();
				RenderSystem.enableDepthTest();
			}
		}
	}
	
}

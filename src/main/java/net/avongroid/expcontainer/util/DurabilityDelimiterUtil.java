package net.avongroid.expcontainer.util;

public class DurabilityDelimiterUtil {
	
	public static final int calcDelimiters (int maxCapacity, int capacity, int delimiters) {
		return (int)((double)capacity/(double)maxCapacity*(double)delimiters);
	}
	
}
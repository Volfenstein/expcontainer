package net.avongroid.expcontainer.util;

import net.minecraft.entity.player.PlayerEntity;

public class ExperienceStorageUtil {
	
	public static int getLevelFromXP(int xp) {
		int level = 0;

		while (true) {
			final int xpToNextLevel = getHowMuchXPForLevel(level);
			if (xp < xpToNextLevel) return level;
			++ level;
			xp -= xpToNextLevel;
		}
	}
	
	public static PlayerEntity addPlayerXP(PlayerEntity player, int amount) {
		int xp = getPlayerXP(player) + amount;
		if (xp < 0) {
			player.totalExperience = 0;
			player.experienceLevel = 0;
			player.experienceProgress = 0.0f;
			return player;
		}
		player.totalExperience = xp;
		player.experienceLevel = getLevelFromXP(xp);
		int levelXP = getXPFromLevel(player.experienceLevel);
		player.experienceProgress = (xp - levelXP) / (float)player.getNextLevelExperience();
		return player;
	}
	
	public static int getPlayerXP(PlayerEntity player) {
		return (int)ExperienceStorageUtil.getXPFromLevel(player.experienceLevel) + (int)(player.experienceProgress * (float)player.getNextLevelExperience());
	}
	
	public static int getXPFromLevel(int level) {
		if (level > 0 && level < 16)
			return level * level + 6 * level;
		else if (level > 15 && level < 31)
			return (int) (2.5 * level * level - 40.5 * level + 360);
		else if (level > 30)
			return (int) (4.5 * level * level - 162.5 * level + 2220);
		return 0;
	}
	
	public static int getHowMuchXPForLevel(int level) {
		if (level >= 30)
			return 112 + (level - 30) * 9;
		if (level >= 15)
			return 37 + (level - 15) * 5;
		return 7 + level * 2;
	}
}
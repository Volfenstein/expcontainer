package net.avongroid.expcontainer.util;

public interface ExperienceStorageProvider {
	int getStorageCapacity();
	boolean hasExperienceStorage();
	ExperienceStorage getStorage();
}
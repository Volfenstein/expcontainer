package net.avongroid.expcontainer.util;

import net.minecraft.util.Identifier;

public class Identifiers {
	
	public static Identifier of (final String MODID, final String NAME) {
		return new Identifier(MODID, NAME);
	}
	
	public static Identifier of (final String NAME) {
		return new Identifier(NAME);
	}
	
}
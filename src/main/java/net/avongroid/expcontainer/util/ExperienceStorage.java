package net.avongroid.expcontainer.util;

public class ExperienceStorage {
	
	public static final byte ADD = 0, REMOVE = 1, MULTIPLE = 2;
	
	public static final ExperienceStorage instant (ExperienceStorage storage) {
		return instant(storage.getMaxXP(), storage.getXP());
	}
	
	public static final ExperienceStorage instant (int maxXP, int xp) {
		return new ExperienceStorage(maxXP, xp);
	}
	
	public static final ExperienceStorage instant (int maxXP) {
		return instant(maxXP, 0);
	}
	
	private int maxXP;
	
	public int xp = 0;
	
	public ExperienceStorage(ExperienceStorage storage) {
		this(storage.getMaxXP(), storage.getXP());
	}
	
	public ExperienceStorage(int maxXP, int xp) {
		this.maxXP = maxXP;
		this.sum(xp);
	}
	
	public ExperienceStorage(int maxXP) {
		this(maxXP, 0);
	}
	
	public ExperienceStorage calc(byte operation, int amount) {
		switch (operation) {
		case ADD:
			a(amount);
			break;
		case REMOVE:
			s(amount);
			break;
		case MULTIPLE:
			m(amount);
			break;
		}
		return this;
	}

	public ExperienceStorage multiply(int factor) {
		m(factor);
		return this;
	}

	public ExperienceStorage subtract(int amount) {
		s(amount);
		return this;
	}

	public ExperienceStorage sum(int amount) {
		a(amount);
		return this;
	}

	public void setMaxXP(int amount) {
		this.maxXP = amount;
	}
	
	private void m(int factor) {
		for (int i = 0; i < factor; i++)
			a(xp);
	}
	
	private void s(int amount) {
		if (amount < getXP()) {
			xp -= amount;
		} else {
			xp = 0;
		}
	}
	
	private void a(int amount) {
		xp += amount;
	}
	
	public boolean isEmpty() {
		return getXP() <= 0;
	}
	
	public boolean isFull() {
		return getXP() >= getMaxXP();
	}

	public int getMaxXP() {
		return maxXP;
	}

	public int getXP() {
		return xp;
	}
}
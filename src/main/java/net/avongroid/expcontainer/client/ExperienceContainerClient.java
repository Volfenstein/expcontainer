package net.avongroid.expcontainer.client;

import net.fabricmc.api.ClientModInitializer;

import net.avongroid.expcontainer.util.Identifiers;

import net.avongroid.expcontainer.client.predicate.ExperienceContainerBlockItemHavePredicate;

import net.fabricmc.fabric.api.object.builder.v1.client.model.FabricModelPredicateProviderRegistry;

public class ExperienceContainerClient implements ClientModInitializer {
	
	@Override
	public void onInitializeClient() {
		FabricModelPredicateProviderRegistry.register(Identifiers.of("have"), ExperienceContainerBlockItemHavePredicate::xpPredicate);
	}

}

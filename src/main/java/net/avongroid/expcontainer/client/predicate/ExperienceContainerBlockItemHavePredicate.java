package net.avongroid.expcontainer.client.predicate;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.item.ItemStack;

import net.minecraft.nbt.NbtCompound;

import net.minecraft.entity.LivingEntity;

import net.minecraft.client.world.ClientWorld;

@Environment(EnvType.CLIENT)
public class ExperienceContainerBlockItemHavePredicate {
	public static float xpPredicate (ItemStack stack, ClientWorld world, LivingEntity entity, int i) {
		NbtCompound nbt = stack.getSubNbt("BlockEntityTag");
		return nbt != null && nbt.contains("XP") ? nbt.getInt("XP") > 0 ? 1.0f : 0.0f : 0.0f;
	}
}